<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=jZ]{NKujt;]y/Y*K[%),OZ4J>r.mts-FQ;%jaaaJz:ew!W;kGl}K!Sb10uNS[@]');
define('SECURE_AUTH_KEY',  'MkRX&x[7/)[L^-u[V*8!(7L3(r(UIsVLzY:r]|P?XE(;=J~ckk,|G)6+wuqb$08O');
define('LOGGED_IN_KEY',    'l)/ct06jw|.f<m,^f53qZzCj+D518aH@r4H_(^hM07P2LEYbE9w:@.2bl^T#@#3D');
define('NONCE_KEY',        'j.$n&z.J-J(6?.8wZ(h:QoC`}${tq6@t/GhaD~?h2om_Ms8|14[N{{;}a[m]gi@&');
define('AUTH_SALT',        '}B)BxSuI0xxqF&{D)-f^^(3H4$_Q$g>OxFMEtb@2/~[VUpn]Matc<#Cb_?/l,2(<');
define('SECURE_AUTH_SALT', '>qBcg11n9AQ)CK/nff+70aU%9RVR@oE[EReas%NV`c2yf9;lecHJuT2LK%Z4<S&U');
define('LOGGED_IN_SALT',   'A%--,77C9@SU[:|NNkI?:,%;GcM:k<:$bhS6|~}xC04DGAx*9sip^$XVIo)0gBz2');
define('NONCE_SALT',       '[(`(,Z0J]{5@)t@$ 4f^27l*#Lm75z`embG$]l5W#.>kAbLIA`8N6jao8,;{q-}p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
