			</div>
			<!-- /wrapper -->

			<!-- footer -->
			<footer class="footer" role="contentinfo">

				<!-- copyright -->
				<div class="link-zone">
				<?php if( have_rows('link_list', 'option') ): ?>
							<?php while( have_rows('link_list', 'option') ): the_row();  ?>

								<div class="list">

									<h4 class="title"><?php the_sub_field('title', 'option'); ?></h4>
									<?php if( have_rows('link', 'option') ): ?>

										<?php while( have_rows('link', 'option') ): the_row();  ?>

											<div class="link-list">
												<a href="<?php the_sub_field('link', 'option'); ?>"><?php the_sub_field('title', 'option'); ?></a>
											</div>

										<?php endwhile; ?>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
				<?php endif; ?>
				</div>
				<p class="copyright">
					<?php the_field('copyright', 'option'); ?>
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
