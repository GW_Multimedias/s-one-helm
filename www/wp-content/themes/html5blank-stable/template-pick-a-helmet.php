ç<?php /* Template Name: pick a helmet Template */ get_header();


?>

	<main role="main">
		<!-- section -->
		<section>

			<h1><?php the_title(); ?></h1>


			<div class="top-video">
				<div class=""><?php the_field('top_video'); ?></div>
			</div>
			<div class="content">
				<div class=""><?php the_field('content'); ?></div>
			</div>
			<div class="bottom-img">
				<img src="<?php the_field('bottom_img'); ?>" alt="">
			</div>









		<?php if (have_posts()): while (have_posts()) : the_post(); ?>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
