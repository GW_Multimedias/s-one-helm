<?php /* Template Name: Home Template */ get_header();

$roger = get_field('content');
?>

	<main role="main">
		<!-- section -->
		<section>


			<?php if( have_rows('slider') ): ?>
					<div class="slider-for">

							<?php while( have_rows('slider') ): the_row();  ?>

								<div class="slick-container">
								<?php $image3 = get_sub_field('carousel_img'); ?>
									<img src="<?php echo $image3; ?>"  />
									<div class="slideMobile">
										<div class="slideContent <?php the_sub_field('position_slider'); ?>">
											<h1><?php the_sub_field('carousel_titre'); ?></h1>
											<h6><?php the_sub_field('carousel_texte'); ?></h6>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
					</div>
			<?php endif; ?>

			<div class="content">
					<h1><?php echo $roger['Title']; ?></h1>
					<?php echo $roger['text']; ?>
			</div>
			<div class="coming-soon">
					<div class="">Zone de shop coming soon</div>
			</div>

			<?php

			    $loop = new WP_Query('showposts=5&orderby=ID&order=DESC');
			    if($loop->have_posts()): while($loop->have_posts()): $loop->the_post();
			?>
				<div class="">
				</div>
			    <div class="post" id="post-<?php the_ID(); ?>">
			        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			        <span class="post-meta">
			            <?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?>
			        </span>
			    </div>
			<?php endwhile; else: ?>
			    No recent posts yet!
			<?php endif; ?>











		<?php if (have_posts()): while (have_posts()) : the_post(); ?>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
